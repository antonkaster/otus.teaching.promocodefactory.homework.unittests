﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class LimitRequestBuilder
    {

        public static SetPartnerPromoCodeLimitRequest CreateLimit()
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = new System.DateTime(2022, 1, 1)
            };
        }

        public static SetPartnerPromoCodeLimitRequest SetWrongLimit(this SetPartnerPromoCodeLimitRequest limitRequest)
        {
            limitRequest.Limit = -1;
            return limitRequest;
        }
    }
}
